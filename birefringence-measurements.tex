
% chktex-file 8 
% chktex-file 45 
% chktex-file 46

\section{Birefringence Measurements}

\subsection{Intensity Oscillations}\label{sec:intensity-oscillations}

This measurement is taken by evaluating the transmittance $\left(tT = \frac{I_\perp}{I_\perp + I_\parallel}\right)$ values at an arbitrary point ((600, 100) and (10, 10)) and the image average, across $360^\circ$ rotations of the polarizer and analyzer. Typically, one would expect such sinusoidal curves to represent birefringence, however, they are $180^\circ$ period, whereas birefringence is $90^\circ$ periodic. Additionally, these curves are not quite sinusoidal: the lower curve is wider and the peaks are narrower. Interestingly, in figure~\ref{fig:intensity-oscillations} there are a couple curves with `flattened' troughs, most notably the ``sample SL42c'' and ``no sample'' curves. This is a symptom of clipping in our images, where certain pixels are overexposed in our measurements. This is alleviated with an implementation of HDR, which sequentially takes images of shorter exposures to compensate for the overexposed images, until no pixels are over exposed. The HDR feature is an essential feature that compensates for fluctuating intensities at different polarizer angles.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{intensity-oscillations}
    \caption{Intensity oscillations according to polarizer angle\label{fig:intensity-oscillations}}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{intensity-oscillations-no-sample}
    \caption{Intensity oscillations with no sample\label{fig:intensity-oscillations-no-sample}}
\end{figure}

\subsection{Phase Plate Tests}\label{sec:phase-plate-test}

The goal of these tests is to further investigate the intensity oscillations as well as the experimental setup's sensitivity to changes in polarization. The equipment used are the thorlabs LCC25 (liquid crystal controller) and LCC1423-A (voltage variable phase plate). There are two primary sections to this test:

\begin{enumerate}
    \item At a large phase-change (near 45 degrees), find the minimum detectable increment in voltage.
    \item Repeat the same at a small phase-change. The minimum detectable voltage increment should be higher since the phase change is much smaller.
    \item Using the known minimum detectable voltage increment, determine the behaviour of the system near zero phase-change ($\SI{\sim 7.000}{\volt}$)
\end{enumerate}

Note, the following plots represent an image-wide average. This may present issues as can be seen in figure~\ref{fig:oscillations-5V}, where pixel-to-pixel variance in birefringence approaches or even exceeds the difference between small voltage increments.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{oscillations-2V-to-8V.png}
    \caption{Oscillation plots with phase plate voltage from 2V to 8V\label{fig:oscillations-2V-to-8V}}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{oscillations-5V.png}
    \caption{Oscillation plots of mean and individual pixels at 5V\label{fig:oscillations-5V}}
\end{figure}

Figure~\ref{fig:oscillations-2V-to-8V} shows the data from a wide range of measurements to help discern approximate voltages that correspond to a maximum and minimum retardance, 2V and 7V respectively. At these extrema, I took measurements of sequentially smaller increments in voltage to determine the sensitivity of the system. Figure~\ref{fig:oscillations-2V} shows that at high birefringence values near 2V, even a 0.001 increment in voltage shows a distinguishable level of birefringence. However, at smaller birefringence values, a 0.001V or even a 0.01V increment is hard to distinguish, as seen in figure~\ref{fig:oscillations-7V}. Take note that an `empty' curve is also included, meaning there is no sample in beam. Comparing the two curves, the `empty' curve has the bottom-heavy curve as described in~\ref{sec:intensity-oscillations}, whereas the 7V curves all have a similar `double' sinusoidal curve, in the general form of $f(x) = \sin{x}\cos{x}$, where both are also $180^\circ$ periodic.

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{oscillations-near-2V.png}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{oscillations-near-2V-zoom.png}
    \end{subfigure}
    \caption{Intensity oscillation plots near 2V\label{fig:oscillations-2V}}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{oscillations-near-7V.png}
    \caption{Oscillation plots near 7V\label{fig:oscillations-7V}}
\end{figure}

To find the minimum with improved precision, I performed more detailed increments in voltage near 7V. Figure~\ref{fig:oscillations-6V-to-7V} shows that 6.5V is closer to the minimum.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{oscillations-6V-to-7V.png}
    \caption{Oscillation plots with voltages between 6.0V and 7.5V\label{fig:oscillations-6V-to-7V}}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{oscillations-6V-to-7V-subtract-empty.png}
        \caption{Normalized by subtracting\label{fig:oscillations-6V-to-7V-norm-subtract}}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{oscillations-6V-to-7V-divide-empty.png}
        \caption{Normalized by diving\label{fig:oscillations-6V-to-7V-norm-divide}}
    \end{subfigure}
    \caption{'Normalized' oscillation plots between 6.0V and 7.5V\label{fig:oscillations-6V-to-7V-norm}}
\end{figure}

The plots in figure~\ref{fig:oscillations-6V-to-7V-norm} are an attempt to normalize the data by either subtracting (figure~\ref{fig:oscillations-6V-to-7V-norm-subtract}) or dividing (figure~\ref{fig:oscillations-6V-to-7V-norm-divide}) by the `no sample' oscillation profile.

Notice that in figure~\ref{fig:oscillations-6V-to-7V}, there is a very strange and unexpected behaviour in which the oscillation peaks are translated. This suggests a change in birefringence axis angle (not the intensity of birefringence/polarization rotation). This only occurs at smaller values of birefringence and not at greater amounts as shown in figure~\ref{fig:oscillations-2V-to-8V}. To investigate this further, I created a 2D intensity map of tT across 5-8V in figure~\ref{fig:phase-plate-voltage-tT-map}, which confirms that this is the case.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{phase-plate-voltage-tT-map.png}
    \caption{Intensity map of transmittance with varying voltages.\label{fig:phase-plate-voltage-tT-map}}
\end{figure}

Looking at the tT map in figure~\ref{fig:phase-plate-voltage-tT-map}, it seems like 6.500V is the closest to 0 retardance or birefringence. Comparing this to an empty beam (no sample or waveplate) measurement in figure~\ref{fig:oscillations-6.5V-empty}, there is some unexplained birefringence in the empty beam. The only explanation for this would be birefringence in the lens/beam expander in the setup. This will be further investigated in Section~\ref{sec:objective-lens-test}.

% TODO: alignment of axis plots and discussion
Another observation to be made in figure~\ref{fig:phase-plate-voltage-tT-map} is a shift in birefringence axis (the peaks are in different locations) as the voltage crosses 6.5V---voltage corresponding to near zero birefringence. This is suspected to be a characteristic of the phase plate, as they shift accordingly with the orientation of the phase plate axis. Figure~\ref{fig:phase-plate-alignment} shows an alignment attempt to match the birefringence axes. Unfortunately, this does not yield any new meaningful information towards identifying the causes of the oscillations.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{6.5-vs-empty-tT}
    \caption{Oscillation plot of an empty beam and `zero' birefringence\label{fig:oscillations-6.5V-empty}}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{align-attempt}
    \caption{Alignment attempt of the phase plate.\label{fig:phase-plate-alignment}}
\end{figure}


\subsection{Objective Lens Test}\label{sec:objective-lens-test}

\subsubsection{Analyzer Place Before and After Lens}\label{sec:analyzer-before-after-lens}

This test aims to determine the birefringent characteristics of the objective lens. However, due to the sensitivity and precision of the lens, it cannot be moved or touched. The procedure consists of 4 measurements and is as follows:
\begin{enumerate}
    \item Baseline control measurement with no sample.
    \item Measurement without analyzer. Important to take note of analyzer position before dismounting.
    \item Place analyzer before the lens.
    \item Replace analyzer to original location and remeasure and compare with control to see if there are unexpected changes due to imperfect placement.
\end{enumerate}
These measurements can be seen in figure~\ref{fig:lens-tT-4-measurements}. The align and cross measurement data are also included.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{tT-4-measurements}
    \caption{tT measurements before and after the lens\label{fig:lens-tT-4-measurements}}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{align-before-after}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{cross-before-after}
    \end{subfigure}
    \caption{Intensity measurements with analyzer before vs after the lens}
\end{figure}\label{fig:before-after-lens}

A test was repeated using a power meter for redundancy purposes and the results are quite similar. Table~\ref{tab:analyzer-before-lens} shows a sample measurement.
\begin{table}[H]
    \centering
    \begin{tabular}{@{}cc@{}}
        \toprule
        Polarizer Angle & Power ($\mu$W) \\ \midrule
        0               & 12.13          \\
        15              & 10.56          \\
        30              & 10.73          \\
        45              & 12.64          \\
        60              & 15.71          \\
        75              & 19.48          \\
        90              & 22.60          \\
        105             & 24.52          \\
        120             & 24.38          \\
        135             & 22.30          \\
        150             & 18.82          \\
        165             & 15.15          \\
        180             & 12.03          \\
        195             & 10.42          \\
        210             & 10.51          \\
        225             & 12.37          \\
        240             & 15.52          \\
        255             & 19.00          \\
        270             & 22.06          \\
        285             & 23.89          \\
        300             & 23.75          \\
        315             & 21.74          \\
        330             & 18.46          \\
        345             & 15.00          \\
        360             & 12.14          \\ \bottomrule
    \end{tabular}
    \begin{tabular}{@{}cc@{}}
        \toprule
        Polarizer Angle & Power (nW) \\ \midrule
        0               & 26.4       \\
        15              & 27.4       \\
        30              & 28.0       \\
        45              & 28.5       \\
        60              & 28.4       \\
        75              & 28.3       \\
        90              & 27.6       \\
        105             & 27.3       \\
        120             & 26.9       \\
        135             & 26.7       \\
        150             & 26.2       \\
        165             & 25.8       \\
        180             & 25.4       \\
        195             & 24.6       \\
        210             & 24.7       \\
        225             & 25.6       \\
        240             & 26.7       \\
        255             & 27.0       \\
        270             & 26.7       \\
        285             & 26.8       \\
        300             & 26.5       \\
        315             & 26.1       \\
        330             & 26.0       \\
        345             & 25.9       \\
        360             & 26.3       \\ \bottomrule
    \end{tabular}
    \caption{Power measurement data with polarizer and analyzer placed before objective lens. Left is align, right is cross.\label{tab:analyzer-before-lens}}
\end{table}


\subsubsection{Polarizer Analyzer Calibration}\label{sec:polarizer-analyzer-calibration}

% TODO: cite polarizer extinction ratio
Another possible source of these unexpected results can be a misalignment of the polarizer and analyzer. This means that when they are aligned, they will not have 100\% transmission and when cross, they have a non-zero transmission. Important to note that the wire grid polarizers used only have an extinction ratio of about 800. Therefore, there exists a maximum and minimum theoretical transmittance. The calibration is performed by looping through a set of polarizer angles, where at each polarizer angle, the analyzer rotates from $0^\circ-360^\circ$. For each polarizer/analyzer configuration, the intensity is recorded by the power meter. At every polarizer angle, the intensity data is fitted to a cosine curve against the analyzer angle. The phase shift (the term $\phi$ in $f(x) = a + b\cos{kx - \phi}$) is the offset angle between the polarizer and analyzer.

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.95\textwidth]{calibration-before-lens}
        \caption{Analyzer placed before lens}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.95\textwidth]{calibration-after-lens}
        \caption{Analyzer placed after lens}
    \end{subfigure}
    \caption{Calibration of polarizer-analyzer\label{fig:polarizer-analyzer-calibration}}
\end{figure}

% TODO: double check the offset value. 
The curve fit on the data sets yields a phase shift of $\SI{7.10(2)}{^\circ}$. Moving forwards, this is the offset value to be used for all measurements.

\subsubsection{Post-Calibration Objective Lens Test}\label{sec:post-calibration-objective-lens-test}

This section is simply a repeat of the measurements from Section~\ref{sec:objective-lens-test} with the calibrated analyzer and polarizer. The results can be seen in figure~\ref{fig:before-after-lens-lens-post-calibration}.

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{align-post-calibration}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{cross-post-calibration}
    \end{subfigure}
    \caption{Intensity measurements with analyzer before vs after the lens, with calibrated polarizers}
\end{figure}\label{fig:before-after-lens-lens-post-calibration}

As one may expect from a calibration, the measurements are slightly different. However, upon a closer inspection of figure~\ref{fig:before-after-lens}, there seems to be more variance than can be explained by simply calibration. Plotting the ratios of measurements with the analyzer before divided by the analyzer after the lens may yield better insight into this issue. Figure~\ref{fig:ratio-before-after-lens} shows two of the same plot: the only exception being that the second plot represents a second identical measurement, with all conditions held constant. Immediately, their difference is apparent, and that their must be some very strange underlying behaviour. This behaviour is investigated further in Section~\ref{sec:repeat-lens-test}.

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{ratio-post-calibration}
        \subcaption{First ratio plot}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{ratio-post-calibration-2}
        \subcaption{Second ratio plot}
    \end{subfigure}
    \caption{Ratio plots of analyzer before lens divided by analyzer after lens}\label{fig:ratio-before-after-lens}
\end{figure}

% TODO: Back-to-back measurements 
\subsubsection{Repeat Lens Test}\label{sec:repeat-lens-test}

This section describes the tests performed to identify the run-to-run variance observed in Section~\ref{sec:post-calibration-objective-lens-test}. The order of the measurements are as follows:

\begin{enumerate}
    \item Camera v1 (previous day)
    \item Power meter v1
    \item Cam v2
    \item Cam v3
    \item Power meter v2
\end{enumerate}

With the exception of the first measurement done with a camera, all other measurements were conducted sequentially, within a time span of about an hour.

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{camera-align}
        \subcaption{Camera-Align}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{powermeter-align}
        \subcaption{Powermeter-Align}
    \end{subfigure}

    \medskip
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{camera-cross}
        \subcaption{Camera-Cross}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{powermeter-cross}
        \subcaption{Powermeter-Cross}
    \end{subfigure}
    \medskip
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{camera-ratio}
        \subcaption{Camera-Ratio}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{powermeter-ratio}
        \subcaption{Powermeter-Ratio}
    \end{subfigure}
    \caption{Repeat lens tests. The first tests (v1) are in blue, the second tests (v2) are in red, and the third tests (v3), if applicable, are in green. For the align and cross plots, the solid lines represents measurements with the analyzer placed before the lens, whereas the dashed lines is with the analyzer placed after the lens.}\label{fig:repeat-lens-tests}
\end{figure}

\subsubsection{Fourier Transform Method}

% Cite Information
Typically, to extract birefringence from the Transmittance (tT) data, a curve fit to a cosine or the transmittance function is required. However, performing a curve fit on a per pixel basis would require a total of $1216\times1936 = \num{2354176}$. An alternative to curve fitting is to perform a fourier transform on the data set and try to extract the birefringence data there.

There exists an extremely efficient algorithm for computing a discrete fourier transform (DFT) or its inverse (IDFT), called the fast fourier transform (FFT). So instead of requiring nearly an hour to determine birefringence, a mere few seconds is needed. A fourier transform converts data from its original domain, into a frequency domain. From there, it is possible to extract the birefringence, which has a period of $\SI{90}{\degree}$, corresponding to a frequency of 4 periods per full $\SI{360}{\degree}$ rotation of the polarizer. Birefringence can be obtained by normalizing the peak with a frequency of 4 against the 0\textsuperscript{th} order peak (which represents the mean of the data): $\beta = \frac{2 * a_{f=4}}{a_0}$.

This method is not only faster and more computationally efficient, it also has the ability to isolate noise from other frequencies and only extract the birefringence.

