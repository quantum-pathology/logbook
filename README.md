# Logbook

## Description
This is an experimental setup using polarimetry to analyze tumorous tissue. This repository contains relevant documentation of work done on this project. Please see [logbook](logbook.pdf) for more details on current progress. 